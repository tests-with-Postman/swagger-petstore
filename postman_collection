{
	"info": {
		"_postman_id": "aa48e733-291e-4ea0-810d-834494d17d5d",
		"name": "PETSTORE",
		"description": "[https://petstore.swagger.io/#/](https://petstore.swagger.io/#/)\n\nThe tests are focused on the \"pet\" endpoint.\n\n<img src=\"https://content.pstmn.io/cb90dd2d-a11e-4eec-8131-0852d4325b5f/aW1hZ2UucG5n\" width=\"1349\" height=\"470\">\n\nTests of the CRUD:\n\n| Verbs | Status + Comment |\n| --- | --- |\n| POST | ❌ Return code is 200. Overflow on ID |\n| POST (duplicated) | ❌ Not possible to duplicate, no ID on body |\n| POST (bad request 400) | ❌ Error, no mandatory fields applied |\n| POST (upload image) | ❌ It must be a PUT method |\n| POST (for updating) | ❌ It must be a PUT verb |\n| GET (consult) | ✅ (Remember overflow on ID) |\n| GET (list all elements) | ❌ Not implemented. |\n| GET (status filter) | ✅ |\n| PUT | ❌ Method has no parameter & is not allowed |\n| PUT (pet does not exist) | ❌ Method has no parameter & is not allowed |\n| DELETE | ✅(Remember overflow on ID) |\n| DELETE (KO) | ✅ |\n\n> BUGS: [https://jiramav.atlassian.net/jira/software/c/projects/PETSTORE/issues](https://jiramav.atlassian.net/jira/software/c/projects/PETSTORE/issues)",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "32415112"
	},
	"item": [
		{
			"name": "PET endpoint",
			"item": [
				{
					"name": "Create PET (KO bad request)",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 400\", function () {\r",
									"  pm.response.to.have.status(400);\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"animal\": \"cat\",\r\n  \"photoUrls\": []\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{pet}}",
							"host": [
								"{{pet}}"
							]
						},
						"description": "Body of the request is not correct.\n\n- BUG: [https://jiramav.atlassian.net/browse/PETSTORE-3](https://jiramav.atlassian.net/browse/PETSTORE-3)"
					},
					"response": []
				},
				{
					"name": "Create PET",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 201\", function () {\r",
									"  pm.response.to.have.status(201);\r",
									"});\r",
									"\r",
									"const jsonData = pm.response.json();\r",
									"\r",
									"pm.test(\"Mandatory fields are present\", function () {\r",
									"\r",
									"  pm.expect(Object.keys(jsonData).length, \"WRONG NUMBER OF PARAMETERS\").to.eql(5);\r",
									"\r",
									"  pm.expect(jsonData.id).to.exist;\r",
									"  pm.expect(jsonData.name).to.exist;\r",
									"  pm.expect(jsonData.photoUrls).to.exist;\r",
									"  pm.expect(jsonData.tags).to.exist;\r",
									"  pm.expect(jsonData.status, \"STATUS FIELD\").to.eql(\"available\");\r",
									"\r",
									"});\r",
									"\r",
									"// Data retrieved for other requests\r",
									"pm.collectionVariables.set(\"pet_id\", jsonData.id);\r",
									"pm.collectionVariables.set(\"pet_name\", jsonData.name);\r",
									"\r",
									"console.log(jsonData.id);\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"name\": \"{{$randomFirstName}}\",\r\n  \"photoUrls\": [\r\n    \"string\"\r\n  ],\r\n  \"status\": \"available\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{pet}}",
							"host": [
								"{{pet}}"
							]
						},
						"description": "Happy path of the pet creation case. Test capture data to use in others requests.\n\n- BUG: [https://jiramav.atlassian.net/browse/PETSTORE-4](https://jiramav.atlassian.net/browse/PETSTORE-4)\n    \n- BUG: [https://jiramav.atlassian.net/browse/PETSTORE-6](https://jiramav.atlassian.net/browse/PETSTORE-6)"
					},
					"response": []
				},
				{
					"name": "Create PET (KO duplicated)",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 400\", function () {\r",
									"  pm.response.to.have.status(400);\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"name\": \"{{pet_name}}\",\r\n  \"photoUrls\": [\r\n    \"string\"\r\n  ],\r\n  \"status\": \"available\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{pet}}",
							"host": [
								"{{pet}}"
							]
						},
						"description": "This request tries to duplicate a pet already existing in the system.\n\n> BUG: [https://jiramav.atlassian.net/browse/PETSTORE-3](https://jiramav.atlassian.net/browse/PETSTORE-3)"
					},
					"response": []
				},
				{
					"name": "Retrieve pets by status: available",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"pm.test(\"Check statuses\", function () {\r",
									"  pm.expect(pm.response.text()).not.to.include(\"pending\");\r",
									"  pm.expect(pm.response.text()).not.to.include(\"sold\");\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{pet}}/findByStatus?status=available",
							"host": [
								"{{pet}}"
							],
							"path": [
								"findByStatus"
							],
							"query": [
								{
									"key": "status",
									"value": "available"
								}
							]
						},
						"description": "Request that filter results over the parameter \"status\". Test search another status on the response."
					},
					"response": []
				},
				{
					"name": "Retrieve pets by status: pending",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"pm.test(\"Check statuses\", function () {\r",
									"  pm.expect(pm.response.text()).not.to.include(\"available\");\r",
									"  pm.expect(pm.response.text()).not.to.include(\"sold\");\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{pet}}/findByStatus?status=pending",
							"host": [
								"{{pet}}"
							],
							"path": [
								"findByStatus"
							],
							"query": [
								{
									"key": "status",
									"value": "pending"
								}
							]
						},
						"description": "Request that filter results over the parameter \"status\". Test search another status on the response."
					},
					"response": []
				},
				{
					"name": "Retrieve pets by status: sold",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"pm.test(\"Check statuses\", function () {\r",
									"  pm.expect(pm.response.text()).not.to.include(\"available\");\r",
									"  pm.expect(pm.response.text()).not.to.include(\"pending\");\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{pet}}/findByStatus?status=sold",
							"host": [
								"{{pet}}"
							],
							"path": [
								"findByStatus"
							],
							"query": [
								{
									"key": "status",
									"value": "sold"
								}
							]
						},
						"description": "Request that filter results over the parameter \"status\". Test search another status on the response."
					},
					"response": []
				},
				{
					"name": "Update pet",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"const jsonData = pm.response.json();\r",
									"\r",
									"pm.test(\"Mandatory fields are present\", function () {\r",
									"\r",
									"  pm.expect(Object.keys(jsonData).length, \"WRONG NUMBER OF PARAMETERS\").to.eql(5);\r",
									"\r",
									"  pm.expect(jsonData.id).to.exist;\r",
									"  pm.expect(jsonData.name).to.exist;\r",
									"  pm.expect(jsonData.photoUrls).to.exist;\r",
									"  pm.expect(jsonData.tags).to.exist;\r",
									"  pm.expect(jsonData.status, \"STATUS FIELD\").to.eql(\"sold\");\r",
									"\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "PUT",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"name\": \"{{pet_name}}\",\r\n  \"status\": \"sold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{pet}}/:pet_id",
							"host": [
								"{{pet}}"
							],
							"path": [
								":pet_id"
							],
							"variable": [
								{
									"key": "pet_id",
									"value": "{{pet_id}}"
								}
							]
						},
						"description": "It tries to modify the previous pet just created in the collection. Not versioned.\n\n> BUG: [https://jiramav.atlassian.net/browse/PETSTORE-5](https://jiramav.atlassian.net/browse/PETSTORE-5)"
					},
					"response": []
				},
				{
					"name": "Update pet (KO)",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"const jsonData = pm.response.json();\r",
									"\r",
									"pm.test(\"Mandatory fields are present\", function () {\r",
									"\r",
									"  pm.expect(Object.keys(jsonData).length, \"WRONG NUMBER OF PARAMETERS\").to.eql(5);\r",
									"\r",
									"  pm.expect(jsonData.id).to.exist;\r",
									"  pm.expect(jsonData.name).to.exist;\r",
									"  pm.expect(jsonData.photoUrls).to.exist;\r",
									"  pm.expect(jsonData.tags).to.exist;\r",
									"  pm.expect(jsonData.status, \"STATUS FIELD\").to.eql(\"sold\");\r",
									"\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "PUT",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"name\": \"{{pet_name}}\",\r\n  \"status\": \"sold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{pet}}/:pet_id",
							"host": [
								"{{pet}}"
							],
							"path": [
								":pet_id"
							],
							"variable": [
								{
									"key": "pet_id",
									"value": "{{$randomInt}}"
								}
							]
						},
						"description": "Pet does not exist, so no updating is possible."
					},
					"response": []
				},
				{
					"name": "Retrieve pet by ID",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"const jsonData = pm.response.json();\r",
									"\r",
									"pm.test(\"All fields are present and data are consistent\", function () {\r",
									"\r",
									"  pm.expect(Object.keys(jsonData).length, \"WRONG NUMBER OF PARAMETERS\").to.eql(6);\r",
									"\r",
									"  pm.expect(jsonData.id, \"ID FIELD\").to.eql(pm.collectionVariables.get(\"pet_id\"));\r",
									"  pm.expect(jsonData.category).to.exist;\r",
									"  pm.expect(jsonData.name).to.exist;\r",
									"  pm.expect(jsonData.photoUrls).to.exist;\r",
									"  pm.expect(jsonData.tags).to.exist;\r",
									"  pm.expect(jsonData.status, \"STATUS FIELD\").to.eql(\"sold\");\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{pet}}/:pet_id",
							"host": [
								"{{pet}}"
							],
							"path": [
								":pet_id"
							],
							"variable": [
								{
									"key": "pet_id",
									"value": "{{pet_id}}"
								}
							]
						},
						"description": "Retrieving an existing pet with all data."
					},
					"response": []
				},
				{
					"name": "Delete pet",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"  pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"const jsonData = pm.response.json();\r",
									"\r",
									"pm.test(\"All fields are present and data are consistent\", function () {\r",
									"\r",
									"  pm.expect(Object.keys(jsonData).length, \"WRONG NUMBER OF PARAMETERS\").to.eql(3);\r",
									"\r",
									"  pm.expect(jsonData.code, \"CODE FIELD\").to.eql(200);\r",
									"  pm.expect(jsonData.type, \"TYPE FIELD\").to.exist;\r",
									"  pm.expect(jsonData.message, \"STATUS FIELD\").to.exist;\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "{{pet}}/:pet_id",
							"host": [
								"{{pet}}"
							],
							"path": [
								":pet_id"
							],
							"variable": [
								{
									"key": "pet_id",
									"value": "{{pet_id}}"
								}
							]
						},
						"description": "Erasing an existing pet."
					},
					"response": []
				},
				{
					"name": "Retrieve pet by ID (KO)",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 404\", function () {\r",
									"  pm.response.to.have.status(404);\r",
									"});\r",
									"\r",
									"const jsonData = pm.response.json();\r",
									"\r",
									"pm.test(\"All error fields are present\", function () {\r",
									"\r",
									"  pm.expect(Object.keys(jsonData).length, \"WRONG NUMBER OF PARAMETERS\").to.eql(3);\r",
									"\r",
									"  pm.expect(jsonData.code, \"CODE FIELD\").to.eql(1);\r",
									"  pm.expect(jsonData.type, \"TYPE FIELD\").to.eql(\"error\");\r",
									"  pm.expect(jsonData.message, \"MESSAGE FIELD\").to.eql(\"Pet not found\");\r",
									"\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{pet}}/:pet_id",
							"host": [
								"{{pet}}"
							],
							"path": [
								":pet_id"
							],
							"variable": [
								{
									"key": "pet_id",
									"value": "{{pet_id}}"
								}
							]
						},
						"description": "Test tries to recover a non existing pet (it was created and deleted previously)."
					},
					"response": []
				},
				{
					"name": "Delete pet (KO)",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 404\", function () {\r",
									"  pm.response.to.have.status(404);\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "{{pet}}/:pet_id",
							"host": [
								"{{pet}}"
							],
							"path": [
								":pet_id"
							],
							"variable": [
								{
									"key": "pet_id",
									"value": "{{pet_id}}"
								}
							]
						},
						"description": "Test tries to delete an unexisting pet."
					},
					"response": []
				}
			]
		},
		{
			"name": "USER endpoint",
			"item": [
				{
					"name": "Create user: ID=1047",
					"event": [
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									"pm.collectionVariables.set(\"Id\", 1047);\r",
									"pm.collectionVariables.set(\"Username\", \"maricarmen_perez\");\r",
									"\r",
									"pm.collectionVariables.set(\"Name\", \"{{$randomFirstName}}\");\r",
									"pm.collectionVariables.set(\"Lastname\", \"{{$randomLastName}}\");\r",
									"pm.collectionVariables.set(\"Email\", \"{{$randomEmail}}\");\r",
									"pm.collectionVariables.set(\"Password\", \"{{$randomInt}}\");\r",
									"pm.collectionVariables.set(\"Phone\", \"{{$randomPhoneNumber}}\");\r",
									"pm.collectionVariables.set(\"Status\", \"{{$randomInt}}\");"
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 201\", function () {\r",
									"    pm.response.to.have.status(201);\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"id\": {{Id}},\r\n  \"username\": \"{{Username}}\",\r\n  \"firstName\": \"{{Name}}\",\r\n  \"lastName\": \"{{Lastname}}\",\r\n  \"email\": \"{{Email}}\",\r\n  \"password\": \"{{Password}}\",\r\n  \"phone\": \"{{Phone}}\",\r\n  \"userStatus\": \"{{Status}}\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{user}}",
							"host": [
								"{{user}}"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get user: ID=1047",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"    pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"pm.test(\"Id & Username are correct\", function () {\r",
									"    var jsonData = pm.response.json();\r",
									"    pm.expect(jsonData.id).to.eql(pm.collectionVariables.get(\"Id\"));\r",
									"    pm.expect(jsonData.username).to.eql(pm.collectionVariables.get(\"Username\"));\r",
									"});"
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{user}}/:username",
							"host": [
								"{{user}}"
							],
							"path": [
								":username"
							],
							"variable": [
								{
									"key": "username",
									"value": "{{Username}}"
								}
							]
						}
					},
					"response": []
				},
				{
					"name": "Update user: ID=1047",
					"event": [
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									"pm.collectionVariables.set(\"Password\", \"654321\");\r",
									"pm.collectionVariables.set(\"Phone\", \"600000000\");"
								],
								"type": "text/javascript",
								"packages": {}
							}
						},
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"    pm.response.to.have.status(200);\r",
									"});\r",
									""
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "PUT",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n  \"password\": \"{{Password}}\",\r\n  \"phone\": \"{{Phone}}\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{user}}/:username",
							"host": [
								"{{user}}"
							],
							"path": [
								":username"
							],
							"variable": [
								{
									"key": "username",
									"value": "{{Username}}"
								}
							]
						}
					},
					"response": []
				},
				{
					"name": "Get user: ID=1047 (user updated)",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Status code is 200\", function () {\r",
									"    pm.response.to.have.status(200);\r",
									"});\r",
									"\r",
									"if (pm.response.to.have.status(200)) {\r",
									"\r",
									"    var jsonData = pm.response.json();\r",
									"\r",
									"    pm.test(\"Id is correct\", function () {\r",
									"        pm.expect(jsonData.id).to.eql(pm.collectionVariables.get(\"Id\"));\r",
									"    });\r",
									"\r",
									"    pm.test(\"Username is correct\", function () {\r",
									"        var aux = pm.collectionVariables.get(\"Username\");\r",
									"        pm.expect(jsonData.username).to.eql(aux);\r",
									"    });\r",
									"\r",
									"    pm.test(\"Password is correct\", function () {\r",
									"        pm.expect(jsonData.password).to.eql(pm.collectionVariables.get(\"Password\"));\r",
									"    });\r",
									"\r",
									"    pm.test(\"Phone is correct\", function () {\r",
									"        pm.expect(jsonData.phone).to.eql(pm.collectionVariables.get(\"Phone\"));\r",
									"    });\r",
									"\r",
									"}"
								],
								"type": "text/javascript",
								"packages": {}
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{user}}/:username",
							"host": [
								"{{user}}"
							],
							"path": [
								":username"
							],
							"variable": [
								{
									"key": "username",
									"value": "{{Username}}"
								}
							]
						}
					},
					"response": []
				}
			]
		}
	],
	"event": [
		{
			"listen": "prerequest",
			"script": {
				"type": "text/javascript",
				"packages": {},
				"exec": [
					""
				]
			}
		},
		{
			"listen": "test",
			"script": {
				"type": "text/javascript",
				"packages": {},
				"exec": [
					""
				]
			}
		}
	],
	"variable": [
		{
			"key": "pet_id",
			"value": ""
		},
		{
			"key": "pet_name",
			"value": ""
		}
	]
}